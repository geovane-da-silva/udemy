import { Component, OnInit } from '@angular/core';
import { HeroesService, Heroe } from '../../services/heroes.service'

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {
	mostrar = false;
  show = true;
	heroes:Heroe[] =[];


	constructor ( private _heroesService:HeroesService ) {
		console.log("soy un constructor")

	}

  ngOnInit() {
  	this.heroes = this._heroesService.getHeroes();

  	console.log( this.heroes );



  }

}
